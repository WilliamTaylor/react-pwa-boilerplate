import React from 'react';

import '../styles/base.scss';

import MyComponent from './MyComponent.jsx';

export default function () {
	return (
		<div className="app">
			<MyComponent />
		</div>
	)
}
